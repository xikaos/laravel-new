FROM php:7.3-stretch

COPY --from=composer:latest /usr/bin/composer /usr/local/bin

RUN groupadd -r deploy && useradd -r -g deploy deploy && \
    mkdir /app && chown deploy:deploy /app

RUN apt-get update && apt-get install -y zip libzip-dev \
  && docker-php-ext-configure zip --with-libzip \
  && docker-php-ext-install zip

RUN composer global require "laravel/installer"

RUN chmod +x /root/.composer/vendor/laravel/installer/bin/laravel
RUN ln -s /root/.composer/vendor/laravel/installer/bin/laravel /usr/local/bin/laravel

RUN laravel new app --force

WORKDIR /app

COPY entrypoint.sh /usr/local/bin/entrypoint.sh

RUN chmod +x /usr/local/bin/entrypoint.sh

ENTRYPOINT ["entrypoint.sh"]