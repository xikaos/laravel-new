# Laravel New

## Purpose
This project was created to keep me out of the hassle of having to `/bin/bash` into a base PHP container, install `composer`, then `laravel/installer` and then creating your brand new app. 

## Usage
1. Build the image inside this directory with `docker build -t laravel-new .`
2. Go to the directory you want to bootstrap laravel.
3. Run `docker run --rm -it -v $(pwd):/out laravel-new`  
4. Wait for composer to install Laravel and its dependencies and **BANG!**, new app on the fly.
5. **IMPORTANT**: The app key is generated in the image build. To avoid key reusage, generate a new key after bootstrapping a new app.